<?php require_once ".\code.php" ;?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Divisibles of Five</h2>
    <?php forLoop() ?>
    <h2>Array Manipulation</h2>
    <?php
    //Activity 2
    //Create an empty array named "students"
    $students = [];
    //Accept a name of a student and add it to the student array
    array_push($students,'John Wick');?>
    <pre><?php
    //Print the names added so far in the student array
    print_r($students);
    //Count the number of names in the student array
    print_r(count($students));
    ?></pre>
    <?php
    //Add another student then print the name and its count
    array_push($students,'Jane Bennett');?>
    <pre><?php print_r($students);
    print_r(count($students));
    //Finally, remove the first student and print the array and its count
    array_shift($students);?>
    <pre><?php
    print_r($students);
    print_r(count($students));
    ?>


</body>
</html>